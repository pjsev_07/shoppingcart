package com.amaysim.test;

import java.util.Iterator;

import com.amaysim.product.Product;
import com.amaysim.shoppingcart.Cart;
import com.amaysim.shoppingcart.ConsolidateItems;

public class CartTest {
	
	public static void main (String[] s) throws Exception {
		
		Product item1 = new Product("ult_small", "Unlimited 1GB", 1, 24.9);
		Product item2 = new Product("ult_medium", "Unlimited 2GB", 1, 29.9);
		Product item3 = new Product("ult_large", "Unlimited 5GB", 1, 44.9);
		Product item4 = new Product("1gb", "1 GB Data-pack", 1, 9.9);
		 
        Cart c = new Cart("Shopping Cart Test");      
        c.addToCart(item1, 3);
        c.addToCart(item4, 1);
        
        ConsolidateItems ci = new ConsolidateItems(c.getCartDetails());                
 
        System.out.println("\nNo. of Product : "+c.productCount());               
        System.out.println ("TOTAL: " + c.getCartPrice());
                
        
        final Iterator it = c.getCartDetails().iterator();
        while(it.hasNext()){
            //System.out.println (it.next());
        }
    }
}
