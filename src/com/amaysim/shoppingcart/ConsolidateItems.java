package com.amaysim.shoppingcart;

import java.util.Collection;
import java.util.Iterator;

public class ConsolidateItems {
	
	Collection items;
	public int totalSmall;
	int totalMedium;
	int totalLarge;
	int totalDataPack;
	
	int buyNforM = 3;
	int forPrice = 2;
	
	public ConsolidateItems(Collection cartDetails) {
		this.items = cartDetails;
		countItems();
	}
	
	private void countItems() {
		final Iterator it = items.iterator();
        while(it.hasNext()){
        	String itemName = it.next().toString();
            if (itemName == "ult_small") {            	
            	totalSmall++;
            }
            if (itemName == "ult_medium") {            	
            	totalMedium++;
            }
            if (itemName == "ult_large") {            	
            	totalLarge++;
            }
            if (itemName == "1gb") {            	
            	totalDataPack++;
            }
        }
       
	}
}
