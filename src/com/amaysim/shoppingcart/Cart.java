package com.amaysim.shoppingcart;

import java.util.*;

import com.amaysim.product.Product;
import com.amaysim.product.ProductNotFoundException;

public class Cart implements ClientOrder {

	public String uid;
	private Map map;

	public Cart(String uid) {
		this.uid = uid;
		map = new HashMap();
	}

	@Override
	public void addToCart(Product p, int qty) {
		if (map.containsKey(p.getCode())) {
			Product p1 = (Product) map.get(p.getCode());
			p1.setPrice(p1.getPrice() + p.getPrice());
			p1.setQty(p1.getQty() + p.getQty());			
		}			
		for (int i=1; i<=qty;i++) {
			int randomNum = 1 + (int)(Math.random() * 100);
			int randomNum2 = 1 + (int)(Math.random() * randomNum);			
			map.put(randomNum2, p);
		}				
	}

	@Override
	public void removeFromCart(String pid) throws ProductNotFoundException {
		if (map.containsKey(pid)) {
			map.remove(pid);			
		} else
			throw new ProductNotFoundException("Product with ID " + pid + " is not Found.");
	}

	public Collection getCartDetails() {
		return map.values();
	}

	@Override
	public Product getProductFromCart(String pid) throws ProductNotFoundException {
		if (map.containsKey(pid)) {
			return (Product) map.get(pid);
		} else {
			throw new ProductNotFoundException("Product with ID " + pid + " is not Found.");
		}
	}

	public int productCount() {
		return map.size();
	}

	@Override
	public double getCartPrice() {
		double price = 0.0d;
		Iterator iterator = getCartDetails().iterator();
		while (iterator.hasNext()) {			
			price += ((Product) iterator.next()).getPrice();
		}
		return price;
	}
}
