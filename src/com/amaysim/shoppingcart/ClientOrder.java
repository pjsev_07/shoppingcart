package com.amaysim.shoppingcart;

import java.util.Collection;

import com.amaysim.product.Product;
import com.amaysim.product.ProductNotFoundException;

public interface ClientOrder {
	
	void addToCart(Product p, int qty);
    void removeFromCart(String pid) throws ProductNotFoundException;
    
    Collection getCartDetails();
    
    Product getProductFromCart(String pid) throws ProductNotFoundException;
    int productCount();
    double getCartPrice();
    
}
